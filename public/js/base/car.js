$(document).ready(function () {
    $("#car-search-submit").click(function (event) {
        event.preventDefault();
        $("#car-carousel").empty();
        searchAvailableCars();
    });
});

function searchAvailableCars() {
    formData = $("#car-filter-form").serialize();
    $.ajax({
        url: Routing.generate('filter'),
        data: {
            formData: formData,
        },
        dataType: 'json',
        success: function (result) {
            if (result.error) {
                alert(result.message);
            }
            result.data.forEach(function (entry) {
                console.log(entry);
                var item = newCarItem(JSON.parse(entry));
                $('#car-carousel').append(item);
            });
        },
        error: function (xhr, status, error) {
            alert('A aparut o eroare, te rugam sa reincerci');
        },
    });
}

function newCarItem(car) {
    console.log(car);
    return "                       <form><div class=\"item-1\" style=\"width: 500.5px ; margin-right: 20px;\">\n" +
        "                            <a href=\"#\"><img src=" + car.image + " style=\"width: 300.5px " + " alt=\"Image\" class=\"img-fluid\"></a>\n" +
        "                            <div class=\"item-1-contents\">\n" +
        "                                <div class=\"text-center\">\n" +
        "                                    <h3><a href=\"#\">" + car.mark + ' ' + car.series + "</a></h3>\n" +
        "                                    <div class=\"rating\">\n" +
        "                                        <span class=\"icon-star text-warning\"></span>\n" +
        "                                        <span class=\"icon-star text-warning\"></span>\n" +
        "                                        <span class=\"icon-star text-warning\"></span>\n" +
        "                                        <span class=\"icon-star text-warning\"></span>\n" +
        "                                        <span class=\"icon-star text-warning\"></span>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"rent-price\"><span>" + car.price + 'RON' + "/</span>day</div>\n" +
        "                                </div>\n" +
        "                                <ul class=\"specs\">\n" +
        "                                    <li>\n" +
        "                                        <span>Doors</span>\n" +
        "                                        <span class=\"spec\">4</span>\n" +
        "                                    </li>\n" +
        "                                    <li>\n" +
        "                                        <span>Seats</span>\n" +
        "                                        <span class=\"spec\">" + car.noOfSeats + "</span>\n" +
        "                                    </li>\n" +
        "                                    <li>\n" +
        "                                        <span>Transmission</span>\n" +
        "                                        <span class=\"spec\">Automatic</span>\n" +
        "                                    </li>\n" +
        "                                    <li>\n" +
        "                                        <span>Minium age</span>\n" +
        "                                        <span class=\"spec\">18 years</span>\n" +
        "                                    </li>\n" +
        "                                </ul>\n" +
        "                                <div class=\"d-flex action\">\n" +
        "                                    <input type='button' value='Rent now' class=\"btn btn-primary\" id='rent-" + car.id + "'>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "                        </div>";
}

$(document).on("click", '[id^="rent-"]', (function (event) {
    console.log('rent');
    console.log($(this).attr('id').split('-')[1]);
    event.preventDefault();
    saveReservation($(this).attr('id').split('-')[1]);
}));

$(document).on("click", '[id^="edit_"]', function () { //  binding click event to all those elements having id starting with edit_
    var index = $(this).attr('id').split("_")[1]; //  extracting the counter from id attribute of edit button
    $("#cancel_" + index).show(); //   using index
});

function saveReservation(carId) {
    $.ajax({
        url: Routing.generate('save-reservation'),
        data: {
            carId: carId,
            formData: $("#car-filter-form").serialize()
        },
        dataType: 'json',
        success: function (result) {
            alert(result.message);
        },
        error: function (xhr, status, error) {
            alert('A aparut o eroare la salvarea rezervarii, te rugam sa reincerci');
        },
    })
}
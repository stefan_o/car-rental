<?php

namespace App\Repository;

use App\Entity\Car;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class ReservationRepository extends EntityRepository
{
    /**
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public function getBetweenDates($fromDate, $toDate)
    {
        return
            $this
                ->createQueryBuilder('r')
                ->andWhere('r.fromDate >= :fromDate')
                ->andWhere('r.toDate <= :toDate')
                ->setParameter('fromDate', $fromDate->format('d-m-Y'))
                ->setParameter('toDate', $toDate->format('d-m-Y'))
                ->getQuery()
                ->getResult();
    }

    /**
     * @return array
     */
    public function listReservations()
    {
        return
            $this
                ->createQueryBuilder('r')
                ->select('u.firstName', 'u.lastName', 'c.mark', 'c.series', 'r.fromDate', 'r.toDate', 'r.approved')
                ->join(User::class, 'u', 'with', 'r.userId = u.id')
                ->join(Car::class, 'c', 'with', 'r.carId = c.id')
                ->getQuery()
                ->getArrayResult();
    }
}
<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class CarRepository extends EntityRepository
{
    /**
     * @param $noOfSeats
     * @param $fuelType
     * @return mixed
     */
    public function getCarsByFilters($noOfSeats, $fuelType)
    {
        return
            $this
                ->createQueryBuilder('c')
                ->andWhere('c.noOfSeats >= :noOfSeats')
                ->andWhere('c.fuel = :fuel')
                ->setParameter('noOfSeats', $noOfSeats)
                ->setParameter('fuel', $fuelType)
                ->getQuery()
                ->getResult();
    }
}
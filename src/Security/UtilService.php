<?php

namespace App\Security;

/**
 * Class UtilService
 * @package App\Security
 */
class UtilService
{
    public static function convertDate(string $date)
    {
        $dateParts = explode('/', $date);

        return $dateParts[1] . '-' . $dateParts[0] . '-' . $dateParts[2];
    }
}
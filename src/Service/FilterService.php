<?php

namespace App\Service;

use App\Entity\Car;
use App\Entity\Reservation;
use App\Repository\CarRepository;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FilterService
 * @package App\Service
 */
class FilterService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getReservationsBetweenDates($data)
    {
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->entityManager->getRepository(Reservation::class);

        $fromDate = (\DateTime::createFromFormat('m/d/Y', $data['fromDate']));
        $toDate = (\DateTime::createFromFormat('m/d/Y', $data['toDate']));
        $toDate->add(new \DateInterval('P1D'));

        $reservations = $reservationRepository->getBetweenDates($fromDate, $toDate);

        return $reservations;
    }

    public function getCarsMatchingFilters($data)
    {
        /** @var CarRepository $carRepository */
        $carRepository = $this->entityManager->getRepository(Car::class);

        $cars = $carRepository->getCarsByFilters($data['numberOfSeats'], strtolower($data['fuelType']));

        return $cars;
    }

    public function getAvailableCars($data)
    {
        $reservations = $this->getReservationsBetweenDates($data);
        /** @var Reservation $reservation */
        $reservedCarsIds = $availableCars = array();
        foreach ($reservations as $reservation) {
            $reservedCarsIds[] = $reservation->getCarId();
        }

        $cars = $this->getCarsMatchingFilters($data);
        /** @var Car $car */
        foreach ($cars as $car) {
            if (!in_array($car->getId(), $reservedCarsIds)) {
                $availableCars[] = json_encode($car);
            }
        }

        return $availableCars;
    }

    public function listAllReservations()
    {
        $reservationRepository = $this->entityManager->getRepository(Reservation::class);
        return $reservationRepository->findAll();
    }
}
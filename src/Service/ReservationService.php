<?php

namespace App\Service;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class ReservationService
 * @package App\Service
 */
class ReservationService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Security */
    private $security;

    /**
     * ReservationService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $data
     * @throws \Doctrine\DBAL\DBALException
     */
    public function saveReservation(array $data)
    {
        $dbh = $this->entityManager->getConnection();
        $sth = $dbh->prepare(
            "ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");
        $sth->execute();

        $fromDate = (\DateTime::createFromFormat('m/d/Y', $data['fromDate']));
        $toDate = (\DateTime::createFromFormat('m/d/Y', $data['toDate']));

        $reservationRepository = $this->entityManager->getRepository(Reservation::class);
        $reservation = $reservationRepository->findOneBy([], ['id' => 'desc']);
        $lastId = $reservation instanceof Reservation ? $reservation->getId() : 1;

        $reservation = new Reservation();
        $reservation
            ->setId(++$lastId)
            ->setCarId($data['carId'])
            ->setUserId($this->security->getUser()->getId())
            ->setFromDate($fromDate)
            ->setToDate($toDate)
            ->setApproved(0)
            ->setStatus(1);

        $this->entityManager->persist($reservation);
        $this->entityManager->flush();
    }
}
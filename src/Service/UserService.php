<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * UserService constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $this->validateRegisterRequest($request);
        $this->saveUser($request);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    private function validateRegisterRequest(Request $request)
    {
        $params = $request->request->all();

        if (empty($params['username'])) {
            throw new \Exception('Username field is mandatory!');
        }
        if (empty($params['email'])) {
            throw new \Exception('Email field is mandatory!');
        }
        if (empty($params['firstName'])) {
            throw new \Exception('First name field is mandatory!');
        }
        if (empty($params['lastName'])) {
            throw new \Exception('last name field is mandatory!');
        }
        if (empty($params['password'])) {
            throw new \Exception('Password field is mandatory!');
        }
        if (empty($params['confirmPassword'])) {
            throw new \Exception('Password confirmation field is mandatory!');
        }
        if ($params['password'] !== $params['confirmPassword']) {
            throw new \Exception('Entered passwords do not match!');
        }
    }

    /**
     * @param Request $request
     */
    private function saveUser(Request $request)
    {
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy([], ['id' => 'desc']);
        $lastId = $user instanceof User ? $user->getId() : 1;

        $user = new User();
        $user
            ->setId(++$lastId)
            ->setFirstName($request->request->get('firstName'))
            ->setLastName($request->request->get('lastName'))
            ->setEmail($request->request->get('email'))
            ->setUsername($request->request->get('username'))
            ->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $request->request->get('password')
                )
            );

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
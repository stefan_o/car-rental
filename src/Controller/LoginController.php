<?php

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Security $security, AuthenticationUtils $authenticationUtils)
    {
        if ($security->getUser() instanceof UserInterface) {
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Security $security, Request $request, UserService $userService)
    {
        try {
            if ($security->getUser() instanceof UserInterface) {
                return $this->redirectToRoute('home');
            }

            if ($request->getMethod() === Request::METHOD_POST) {
                $userService->register($request);
                return $this->redirectToRoute('home');
            }
        } catch (\Exception $e) {
            return $this->render(
                'register.html.twig',
                array(
                    'error' => $e->getMessage()
                )
            );

        }
        return $this->render('register.html.twig');
    }
}
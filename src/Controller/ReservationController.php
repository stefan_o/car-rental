<?php

namespace App\Controller;

use App\Service\ReservationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReservationController
 * @package App\Controller
 */
class ReservationController extends AbstractController
{
    /**
     * @Route("/save-reservation", name="save-reservation", options={"expose"=true})
     */
    public function saveReservation(ReservationService $reservationService, Request $request)
    {
        try {
            parse_str($request->query->get('formData'), $data);
            $data['carId'] = (int)$request->query->get('carId');
            $reservationService->saveReservation($data);
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'error' => true,
                    'message' => $e->getMessage()
                )
            );
        }

        return new JsonResponse(
            array(
                'error' => false,
                'message' => 'Rezervarea a fost salvata cu succes!'
            )
        );
    }
}
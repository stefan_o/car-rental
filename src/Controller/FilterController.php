<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Service\FilterService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FilterController
 * @package App\Controller
 */
class FilterController extends AbstractController
{
    /**
     * @Route("/filter", name="filter", options={"expose"=true})
     */
    public function filter(Request $request, FilterService $filterService, EntityManagerInterface $em)
    {
        $dbh = $em->getConnection();
        $sth = $dbh->prepare(
            "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS'");
        $sth->execute();

        try {
            parse_str($request->query->get('formData'), $data);
            $availableCars = $filterService->getAvailableCars($data);
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    'error' => true,
                    'message' => $e->getMessage()
                )
            );
        }

        return new JsonResponse(
            array(
                'error' => false,
                'data' => $availableCars
            )
        );
    }

    /**
     * @Route("/list-reservations", name="list-reservations", options={"expose"=true})
     */
    public function list(EntityManagerInterface $entityManager)
    {
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $entityManager->getRepository(Reservation::class);
        try {
            $reservations = $reservationRepository->listReservations();
        } catch (\Exception $e) {
            $this->render(
                'list.html.twig',
                array(
                    'error' => true,
                    'message' => $e->getMessage())
            );
        }

        return $this->render(
            'list.html.twig',
            array(
                'reservations' => $reservations
            )
        );
    }
}
<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function view(EntityManagerInterface $entityManager)
    {
        return $this->render('index.html.twig');
//        return new JsonResponse($a->getName());
    }
}